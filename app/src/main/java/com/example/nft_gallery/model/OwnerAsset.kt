package com.example.nft_gallery.model

data class OwnerAsset(
    val image_url: String?,
    val name: String?,
    val collectionName: String?,
    val description: String?,
    val permalink: String?,
    val contractAddress: String?,
    val tokenId: String?

)