package com.example.nft_gallery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nft_gallery.MainActivity
import com.example.nft_gallery.R
import com.example.nft_gallery.adapter.OwnerAssetAdapter
import com.example.nft_gallery.databinding.FragmentOwnerAssetBinding
import com.example.nft_gallery.remote.GeneralConstants
import com.example.nft_gallery.viewmodel.OwnerAssetFragViewModel

class OwnerAssetFragment : Fragment() {

    private lateinit var viewModel: OwnerAssetFragViewModel
    private lateinit var binding: FragmentOwnerAssetBinding

    private lateinit var ownerAssetAdapter: OwnerAssetAdapter

    private var isScrolling = false
    private var currentItems = 0
    private var totalItems = 0
    private var scrollOutItems = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_owner_asset,
            container,
            false
        )
        binding.lifecycleOwner = this@OwnerAssetFragment
        this@OwnerAssetFragment.viewModel =
            ViewModelProvider(this@OwnerAssetFragment).get(OwnerAssetFragViewModel::class.java)
        binding.viewModel = viewModel


        binding.rvOwnerAsset.apply {
            val gridLayoutManager = GridLayoutManager(context, 2)
            layoutManager = gridLayoutManager
            setHasFixedSize(true)
            ownerAssetAdapter = OwnerAssetAdapter(context, viewModel)
            adapter = ownerAssetAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                        isScrolling = true
                    }
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    currentItems = gridLayoutManager.childCount
                    totalItems = gridLayoutManager.itemCount
                    scrollOutItems = gridLayoutManager.findFirstVisibleItemPosition()
                    if (isScrolling && currentItems + scrollOutItems == totalItems && dy > 0) {
                        //Fetch Data
                        isScrolling = false
                        viewModel.isViewLoading.postValue(true)
                        viewModel.fetchOwnerAssets(
                            GeneralConstants.OWNER, GeneralConstants.JSON_FORMAT, totalItems, 20
                        )
                    }
                }
            })
        }


        viewModel.fetchOwnerAssets(
            GeneralConstants.OWNER, GeneralConstants.JSON_FORMAT, 0, 20
        )

        addIsViewLoadingObserver()
        addOwnerAssetArrayListObserver()
        addSelectedOwnerAssetObserver()

        return binding.root
    }

    private fun addSelectedOwnerAssetObserver() {
        viewModel.selectedOwnerAsset.observe(binding.lifecycleOwner!!, Observer {
            if (it != null) {
                (activity as MainActivity).selectedOwnerAsset = it
                Navigation.findNavController(binding.root).navigate(R.id.go_to_assetDetailFragment)
            }
        })
    }


    private fun addOwnerAssetArrayListObserver() {
        viewModel.ownerAssetArrayList.observe(binding.lifecycleOwner!!, Observer {
            ownerAssetAdapter.refresh(it)
        })
    }

    private fun addIsViewLoadingObserver() {
        viewModel.isViewLoading.observe(binding.lifecycleOwner!!, Observer {
            if (it) {
                (activity as MainActivity).showProgress(true)
            } else {
                (activity as MainActivity).showProgress(false)
            }
        })
    }


}