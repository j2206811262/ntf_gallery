package com.example.nft_gallery.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.nft_gallery.MainActivity
import com.example.nft_gallery.R
import com.example.nft_gallery.databinding.FragmentAssetDetailBinding
import com.example.nft_gallery.model.OwnerAsset
import com.example.nft_gallery.viewmodel.AssetDetailViewModel


class AssetDetailFragment : Fragment() {
    companion object {
        private const val TAG = "AssetDetailFragment"
    }

    private lateinit var binding: FragmentAssetDetailBinding
    private lateinit var viewModel: AssetDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val ownerAsset = (activity as MainActivity).selectedOwnerAsset

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_asset_detail,
            container,
            false
        )

        binding.lifecycleOwner = this@AssetDetailFragment
        this@AssetDetailFragment.viewModel =
            ViewModelProvider(this@AssetDetailFragment).get(AssetDetailViewModel::class.java)
        binding.viewModel = viewModel

        binding.appBar.setNavigationOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.go_to_ownerAssetFragment)
        }
        binding.btnPermalink.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(ownerAsset.permalink))
            startActivity(browserIntent)
        }

        addImageURLObserver()

        initAssetDetailInfo(ownerAsset)

        return binding.root
    }

    private fun addImageURLObserver() {
        viewModel.imageURL.observe(binding.lifecycleOwner!!, Observer {
            if (it != null) {
                Glide
                    .with(requireContext())
                    .load(it)
                    .centerCrop()
                    .into(binding.ivOwnerAsset)
            }
        })

    }

    private fun initAssetDetailInfo(ownerAsset: OwnerAsset) {
        Log.d(TAG, "ContractID: ${ownerAsset.contractAddress}, TokenID: ${ownerAsset.tokenId}")
        viewModel.initAssetDetailInfo(ownerAsset)
    }
}