package com.example.nft_gallery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.nft_gallery.R
import com.example.nft_gallery.model.OwnerAsset
import com.example.nft_gallery.viewmodel.OwnerAssetFragViewModel
import java.util.*

class OwnerAssetAdapter(
    private val mContext: Context,
    private val viewModel: OwnerAssetFragViewModel
) : RecyclerView.Adapter<OwnerAssetAdapter.OwnerAssetViewHolder>() {
    var allOwnerAsset = ArrayList<OwnerAsset>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OwnerAssetViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.cell_asset_info, parent, false)

        return OwnerAssetViewHolder(view)
    }

    override fun onBindViewHolder(holder: OwnerAssetViewHolder, position: Int) {
        if (position < allOwnerAsset.size) {
            allOwnerAsset[position].apply {
                holder.tv_title.text = name
                Glide
                        .with(mContext)
                        .load(image_url)
                        .centerCrop()
                        .into(holder.iv_owner_asset)

                holder.card_view.setOnClickListener {
                    viewModel.onClickCardView(this)
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return allOwnerAsset.size
    }

    fun refresh(areaInfoArrayList: ArrayList<OwnerAsset>) {
        allOwnerAsset.addAll(areaInfoArrayList)
        notifyDataSetChanged()
    }

    class OwnerAssetViewHolder(view: View?) : RecyclerView.ViewHolder(view!!) {

        var tv_title: TextView = itemView.findViewById(R.id.tv_title)
        var iv_owner_asset: ImageView = itemView.findViewById(R.id.iv_owner_asset)
        var card_view: CardView = itemView.findViewById(R.id.card_view)
    }

}