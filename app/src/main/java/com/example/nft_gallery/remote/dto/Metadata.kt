package com.example.nft_gallery.remote.dto

data class Metadata(
    val asset: AssetX,
    val schema: String
)