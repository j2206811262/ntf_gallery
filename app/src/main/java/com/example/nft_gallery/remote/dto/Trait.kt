package com.example.nft_gallery.remote.dto

data class Trait(
    val display_type: Any,
    val max_value: Any,
    val order: Any,
    val trait_count: Int,
    val trait_type: String,
    val value: Any
)