package com.example.nft_gallery.remote.dto

data class DisplayData(
    val card_display_style: String,
    val images: List<String>
)