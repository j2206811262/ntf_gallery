package com.example.nft_gallery.remote.dto

data class BaseAssetResponse(
    val assets: List<Asset>
)