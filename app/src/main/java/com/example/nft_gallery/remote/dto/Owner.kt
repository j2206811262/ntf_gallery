package com.example.nft_gallery.remote.dto

data class Owner(
    val address: String,
    val config: String,
    val discord_id: String,
    val profile_img_url: String,
    val user: UserX
)