package com.example.nft_gallery.remote.dto

data class FeeRecipient(
    val address: String,
    val config: String,
    val discord_id: String,
    val profile_img_url: String,
    val user: Int
)