package com.example.nft_gallery.remote

class RemoteConstants {
    companion object {
        val BASE_URL = "https://api.opensea.io/"
    }
}