package com.example.nft_gallery.remote

class GeneralConstants {
    companion object {
        const val OWNER = "0x960DE9907A2e2f5363646d48D7FB675Cd2892e91"
        const val JSON_FORMAT = "json"
    }
}