package com.example.nft_gallery.remote.dto

data class AssetX(
    val address: String,
    val id: String,
    val quantity: String
)