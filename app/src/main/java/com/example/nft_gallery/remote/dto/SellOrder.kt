package com.example.nft_gallery.remote.dto

data class SellOrder(
    val approved_on_chain: Boolean,
    val base_price: String,
    val bounty_multiple: String,
    val calldata: String,
    val cancelled: Boolean,
    val closing_date: Any,
    val closing_extendable: Boolean,
    val created_date: String,
    val current_bounty: String,
    val current_price: String,
    val exchange: String,
    val expiration_time: Int,
    val extra: String,
    val fee_method: Int,
    val fee_recipient: FeeRecipient,
    val finalized: Boolean,
    val how_to_call: Int,
    val listing_time: Int,
    val maker: Maker,
    val maker_protocol_fee: String,
    val maker_referrer_fee: String,
    val maker_relayer_fee: String,
    val marked_invalid: Boolean,
    val metadata: Metadata,
    val order_hash: String,
    val payment_token: String,
    val payment_token_contract: PaymentTokenContract,
    val prefixed_hash: String,
    val quantity: String,
    val r: String,
    val replacement_pattern: String,
    val s: String,
    val sale_kind: Int,
    val salt: String,
    val side: Int,
    val static_extradata: String,
    val static_target: String,
    val taker: Taker,
    val taker_protocol_fee: String,
    val taker_relayer_fee: String,
    val target: String,
    val v: Int
)