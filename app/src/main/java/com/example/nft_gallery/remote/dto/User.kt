package com.example.nft_gallery.remote.dto

data class User(
    val username: String
)