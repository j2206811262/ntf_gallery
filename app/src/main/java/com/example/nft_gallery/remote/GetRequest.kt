package com.example.nft_gallery.remote

import com.example.nft_gallery.remote.dto.BaseAssetResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GetRequest {
    @GET("/api/v1/assets/")
    fun getAssetByOwner(
        @Query("owner") owner: String,
        @Query("format") format: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Call<BaseAssetResponse>

    @GET("/api/v1/assets/")
    fun getAssetDetail(
        @Path("asset_contract_address") contractAddress: String,
        @Path("token_id") tokenId: String
    ): Call<BaseAssetResponse>

}