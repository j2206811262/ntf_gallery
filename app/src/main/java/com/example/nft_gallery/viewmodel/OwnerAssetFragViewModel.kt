package com.example.nft_gallery.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nft_gallery.manager.DownloadManager
import com.example.nft_gallery.model.OwnerAsset
import kotlinx.coroutines.launch

class OwnerAssetFragViewModel : ViewModel() {
    val ownerAssetArrayList = MutableLiveData<ArrayList<OwnerAsset>>()
    val selectedOwnerAsset = MutableLiveData<OwnerAsset>()

    val isViewLoading = MutableLiveData<Boolean>()


    fun fetchOwnerAssets(owner: String, format: String, offset: Int, limit: Int) {
        viewModelScope.launch {
            isViewLoading.postValue(true)
            ownerAssetArrayList.postValue(
                DownloadManager().downloadAssetByOwnerId(owner, format, offset, limit)
            )
            isViewLoading.postValue(false)
        }
    }

    fun onClickCardView(ownerAsset: OwnerAsset) {
        selectedOwnerAsset.postValue(ownerAsset)
    }
}