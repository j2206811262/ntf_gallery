package com.example.nft_gallery.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nft_gallery.model.OwnerAsset

class AssetDetailViewModel : ViewModel() {
    val collectionName = MutableLiveData<String>()
    val imageURL = MutableLiveData<String>()
    val name = MutableLiveData<String>()
    val description = MutableLiveData<String>()

    fun initAssetDetailInfo(ownerAsset: OwnerAsset) {
        collectionName.postValue(ownerAsset.collectionName)
        name.postValue(ownerAsset.name)
        description.postValue(ownerAsset.description)
        imageURL.postValue(ownerAsset.image_url)
    }
}