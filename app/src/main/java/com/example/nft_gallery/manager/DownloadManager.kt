package com.example.nft_gallery.manager

import android.util.Log
import com.example.nft_gallery.model.OwnerAsset
import com.example.nft_gallery.remote.GetRequest
import com.example.nft_gallery.remote.RemoteConstants
import com.example.nft_gallery.remote.RetrofitClient
import com.example.nft_gallery.remote.dto.BaseAssetResponse
import retrofit2.awaitResponse

class DownloadManager {
    companion object {
        private const val TAG = "DownloadManager"
    }

    suspend fun downloadAssetByOwnerId(owner: String, format: String, offset: Int, limit: Int)
            : ArrayList<OwnerAsset> {
        val tempArrayList = arrayListOf<OwnerAsset>()
        val request = RetrofitClient.createRetrofit(RemoteConstants.BASE_URL)
            .create(GetRequest::class.java)

        val response = request.getAssetByOwner(
            owner,
            format,
            offset,
            limit
        ).awaitResponse()
        Log.d(TAG, "Response Code: ${response.code()}")
        if (response.isSuccessful) {
            Log.d(TAG, "Download Successfully!")
            if (response.body() == null) {
                Log.d(TAG, "Download empty body!")
            } else {
                val myData: BaseAssetResponse = response.body()!!

                for (data in myData.assets) {
                    Log.e(TAG, data.name)
                    val ownerAsset = OwnerAsset(
                        data.image_url,
                        data.name,
                        data.collection.name,
                        data.description,
                        data.permalink,
                        data.asset_contract.address,
                        data.token_id
                    )
                    tempArrayList.add(ownerAsset)

                }
            }
        }
        return tempArrayList
    }


    suspend fun downloadAssetDetail(contractAddress: String, tokenId: String)
            : ArrayList<OwnerAsset> {
        val tempArrayList = arrayListOf<OwnerAsset>()
        val request = RetrofitClient.createRetrofit(RemoteConstants.BASE_URL)
            .create(GetRequest::class.java)

        val response = request.getAssetDetail(
            contractAddress,
            tokenId
        ).awaitResponse()
        Log.d(TAG, "Response Code: ${response.code()}")
        if (response.isSuccessful) {
            val dataPerSecond = HashMap<String, String>()
            Log.d(TAG, "Download Successfully!")
            if (response.body() == null) {
                Log.d(TAG, "Download empty body!")
            } else {
                val myData: BaseAssetResponse = response.body()!!

                for (data in myData.assets) {
                    Log.e(TAG, data.name)
                    val ownerAsset = OwnerAsset(
                        data.image_url,
                        data.name,
                        data.collection.name,
                        data.description,
                        data.permalink,
                        data.asset_contract.address,
                        data.token_id
                    )
                    tempArrayList.add(ownerAsset)

                }
            }
        }
        return tempArrayList
    }
}